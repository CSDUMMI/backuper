#!/bin/env bash

mkdir -p "/media/$3_$2" "/mnt/$3_$2" &&
/usr/bin/systemd-mount --automount=yes --collect /dev/$1 "/media/$3_$2" &&
veracrypt --text --mount "/media/$3_$2/container.vc" "/mnt/$3_$2" --pim 0 --keyfiles "/etc/backuper/keys/$3_$2" --password $(cat "/etc/backuper/keys/$3_$2.key") --protect-hidden no &&
rsync -avz --delete --partial $SSH_USER@$SSH_HOST:$SSH_LOCATION "/mnt/$3_$2" &&
veracrypt --text --dismount "/mnt/$3_$2" &&
/usr/bin/systemd-mount -u "/media/$3_$2" &&
/usr/local/bin/notify-backup-done "$3_$2"
