#!/bin/env python3
import sys
import os
import subprocess
import getpass

idVendor = sys.argv[1]
idProduct = sys.argv[2]
devpath = sys.argv[3]
size = sys.argv[4]
password = getpass.getpass("Enter a keyfile's contents for the encrypted volume: ")

try:
    os.mkdir("/etc/backuper")
    os.mkdir("/etc/backuper/keys")
except FileExistsError:
    print("Config at /etc/backuper exists")

# Mount device
subprocess.run(f'/usr/bin/systemd-mount --automount=yes --collect {devpath} "/media/{idVendor}_{idProduct}"', shell=True, check=True)

# Create keyfile
with open(f"/etc/backuper/{idVendor}_{idProduct}.key", "w") as keyfile:
    keyfile.write(password)
    print(f"Stored keyfile at /etc/backuper/{idVendor}_{idProduct}")

# Create VC volume
subprocess.run(f"veracrypt --text --create /media/{idVendor}_{idProduct}/container.vc --size {size} --password {password} --volume-type normal --encryption AES --hash sha-512 --filesystem exfat --pim 0", shell=True, check=True)

# Add udev rule
with open("/etc/udev/rules.d/80-backup.rules", "a") as rules:
    print('ACTION=="add", SUBSYSTEM=="usb", ATTRS{idProduct}=="' + idProduct + '", ATTRS{idVendor}=="' + idVendor + '", RUN{program}+="/usr/local/bin/backup.sh $kernel$number ' + idProduct + ' ' + idVendor + ' $sys"', file=rules)

subprocess.run(f"/usr/bin/systemd-mount -u /media/{idVendor}_{idProduct}", shell=True, check=True)
