#!/bin/env python3
import sys

if __name__ == "__main__":
    with open("/etc/backuper/log", "a") as log:
        print(f"Done with {sys.argv}", file=log)
